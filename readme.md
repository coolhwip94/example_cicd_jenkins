# Example CICD
> This repo is used as an example to demonstrate CICD pipeline utilizing jenkins


### Ansible
---
- ansible command will be run on a remote host (host will be another lab server)
- demonstrates being able to run ansible plays from jenkins_agent


### Python Script
---
- python script will be ran (simple example of running a script on an agent)
- demonstrates an application or script being ran

#### Configuration
---
- if you are using ansible with a jenkins agent, you need to ensure the host you intend to manage has the jenkins_agent user and public key in allowed_hosts file

- Jenkins Config
	- Dashboard > New Item (name=`example_cicd_pipeline` and `freestyle project`)
        - Check `use alternative credential` and select a gitlab api token credential (if not available then create one)
        - Check `restrict where this project can be run` and enter `agent3` (agent3 has private ssh key in my home lab)
	- Source Code Management
		- Use this repo url : https://gitlab.com/coolhwip94/example_cicd_jenkins
        - Build Triggers > `Poll SCM` we poll a specific branch for changes, and build
	- Build Triggers > `Push to gitlab` also works with gitlab/jenkins integration
        - Build
		- run ansible command
		- run python script
	- Post build > publish status to gitlab

